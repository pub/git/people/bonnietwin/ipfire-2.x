###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2007-2021  IPFire Team <info@ipfire.org>                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

###############################################################################
# Definitions
###############################################################################

include Config

VERSUFIX = ipfire$(KCFG)
MODPATH = /lib/modules/$(KVER)-$(VERSUFIX)/extra/wlan

VER        = fd0b735e2e30d32f4d91497242cf6af288bdd082

THISAPP    = 88x2bu-$(VER)
DL_FILE    = $(THISAPP).tar.gz
DL_FROM    = $(URL_IPFIRE)
DIR_APP    = $(DIR_SRC)/$(THISAPP)
TARGET     = $(DIR_INFO)/$(THISAPP)-kmod-$(KVER)-$(VERSUFIX)

###############################################################################
# Top-level Rules
###############################################################################

objects = $(DL_FILE)

$(DL_FILE) = $(DL_FROM)/$(DL_FILE)

$(DL_FILE)_MD5 = 6c2207213d8bc81309d08b26b4d77eb4

install : $(TARGET)

check : $(patsubst %,$(DIR_CHK)/%,$(objects))

download :$(patsubst %,$(DIR_DL)/%,$(objects))

md5 : $(subst %,%_MD5,$(objects))

dist: 
	$(PAK)

###############################################################################
# Downloading, checking, md5sum
###############################################################################

$(patsubst %,$(DIR_CHK)/%,$(objects)) :
	@$(CHECK)

$(patsubst %,$(DIR_DL)/%,$(objects)) :
	@$(LOAD)

$(subst %,%_MD5,$(objects)) :
	@$(MD5)

###############################################################################
# Installation Details
###############################################################################

$(TARGET) : $(patsubst %,$(DIR_DL)/%,$(objects))
	@$(PREBUILD)
	@rm -rf $(DIR_APP) && cd $(DIR_SRC) && tar axf $(DIR_DL)/$(DL_FILE)
	cd $(DIR_APP) && patch -Np1 < $(DIR_SRC)/src/patches/rtl8822bu/remove-ipx.patch
	cd $(DIR_APP) && CONFIG_RTL8822BU=m make $(MAKETUNING) \
		-C /lib/modules/$(KVER)-$(VERSUFIX)/build/ M=$(DIR_APP)/ modules

	# Install the built kernel modules.
	mkdir -p $(MODPATH)
	cd $(DIR_APP) && for f in $$(ls *.ko); do \
		/lib/modules/$$(uname -r)$(KCFG)/build/scripts/sign-file sha512 \
			/lib/modules/$$(uname -r)$(KCFG)/build/certs/signing_key.pem \
			/lib/modules/$$(uname -r)$(KCFG)/build/certs/signing_key.x509 \
			$$f; \
		xz $$f; \
		install -m 644 $$f.xz $(MODPATH); \
	done

	@rm -rf $(DIR_APP)
	@$(POSTBUILD)

